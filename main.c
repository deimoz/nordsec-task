#include<stdio.h>
#include<sys/inotify.h>
#include<unistd.h>
#include<stdlib.h>
#include<signal.h>
#include<fcntl.h>
#include <string.h>
#include <time.h>
#include <regex.h>

#include "fswatcher.h"
#include "cmdline.h"
#include "log.h"

#define EVENT_SIZE  ( sizeof (struct inotify_event) )
#define BUF_LEN (EVENT_SIZE + 16) * 1024

#define MAX_LINE_LEN 256

int fd, wd;

void sig_handler(int sig) {

    /* Remove the watch descriptor and close the inotify instance*/
    inotify_rm_watch(fd, wd);
    close(fd);
    exit(0);
}

static void initLogFile(const char *path) {
    FILE *logfd = fopen(path, "a+");
    if (logfd < 0) {
        exit(2);
    }
    log_add_fp(logfd, LOG_INFO);
}

static void readLogFile(const char *path, const char* datetime, char* regex) {
    FILE* fp;
    fp = fopen(path, "r");
    if (fp == NULL) {
        perror("Failed: ");
        return;
    }
    regex_t r;

    int result = regcomp (&r, regex, REG_EXTENDED);

    if (result > 0) {
        log_error("failed to parse regex");
        exit(78);
    }

    // preparse the date
    struct tm tm_given;
    void* ret = strptime(datetime, "%Y-%m-%d %H:%M:%S", &tm_given);
    if (ret == NULL) {
        ret = strptime(datetime, "%Y-%m-%d", &tm_given);
        if (ret == NULL) {
            log_error("Failed to parse date");
            exit(34);
        }
    }
    time_t dt1 = mktime(&tm_given);
    char buffer[MAX_LINE_LEN];
    // -1 to allow room for NULL terminator for really long string
    while (fgets(buffer, MAX_LINE_LEN - 1, fp))
    {
        // Remove trailing newline
        buffer[strcspn(buffer, "\n")] = 0;
        // check the date...
        char target[20];
        // size of date, like 2021-08-19 14:05:22 is 19 symbols
        strncpy(target,buffer,19);
        target[19] = '\0';
        struct tm tm;
        // parse the date into something usable
        strptime(target, "%Y-%m-%d %H:%M:%S", &tm);
        time_t dt2 = mktime(&tm);
        double seconds = difftime( dt2, dt1 );
        if (seconds >= 0) {
            // run the regex
            result = regexec (&r, buffer, 0, NULL, 0);
            if (!result) {
                printf("%s\n", buffer);
            }
        }
    }

    fclose(fp);
}

int main(int argc, char *argv[]) {
    struct gengetopt_args_info ai;
    if (cmdline_parser (argc, argv, &ai) != 0) {
        exit(1);
    }

    if (ai.help_given) {
        cmdline_parser_print_help();
        return 0;
    }

    if (!ai.logfile_given) {
        ai.logfile_given = true;
        ai.logfile_arg = "log.txt";
    }
    initLogFile(ai.logfile_arg);

    // check if src and dst are provided, run fswatcher then
    if (ai.sourceDir_given && ai.destinationDir_given) {
        signal(SIGINT, sig_handler);
        fd = inotify_init();
        if (fcntl(fd, F_SETFL) < 0)  // error checking for fcntl
            exit(2);

        wd = initFSWatcherOnDir(fd, ai.sourceDir_arg);
        if (wd == -1) {
            return 13;
        }
        directoryWorker(ai.sourceDir_arg,ai.destinationDir_arg,fd);
        return 0;
    }

    if (!ai.querydate_given || !ai.queryregex_given) {
        log_error("Either date or regex not provided");
    } else {
        readLogFile(ai.logfile_arg,ai.querydate_arg,ai.queryregex_arg);
        return 0;
    }
    cmdline_parser_print_help();
}
