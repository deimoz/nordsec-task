//
// Created by deimoz on 8/19/21.
//

#include "fswatcher.h"

#include <sys/inotify.h>
#include <sys/sendfile.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <stdio.h>
#include <string.h>
#include <malloc.h>

#include "log.h"

#define EVENT_SIZE  ( sizeof (struct inotify_event) )
#define BUF_LEN (EVENT_SIZE + 16) * 1024

bool detect_delete_extension(const char* name) {
    bool result = strncmp("delete_", name, strlen("delete_")) == 0;
    return result;
}

void os_delete_file(const char* folder, const char* format, const char* filename) {
    unsigned long filenameLen = strlen(filename);
    unsigned long source_name_length = strlen(folder);
    char* name = malloc(filenameLen + source_name_length + 1);
    sprintf(name,format,folder,filename);
    remove(name);
    free(name);

    // uzduoties extra : -if the file name is prefixed with 'delete_ISODATETIME_' it should be deleted at the specified time
    // kaip tai realizuoti : uzregistruoti SIGALRM handler, pasizeti prefixa ir issiparse'inti data i unix timestamp, atimti laika nuo dabarties
    // tuomet uzregistuoti su alarm() funkcija, kad po n sekundziu OS issiustu SIGALRM. Tuomet handleryje suveiktu trinimo logika
}

long os_copyfile(const char* source_folder, const char* filename, const char* destination_folder)
{
    int input, output;
    unsigned long filenameLen = strlen(filename);
    // generate source path
    unsigned long source_name_length = strlen(source_folder);
    char* source_name = malloc(filenameLen + source_name_length + 1);
    sprintf(source_name,"%s/%s",source_folder,filename);
    if ((input = open(source_name, O_RDONLY)) == -1)
    {
        return -1;
    }
    // generate destination name
    unsigned long destination_length = strlen(destination_folder);
    char* name = malloc(filenameLen + destination_length + 1);
    sprintf(name,  "%s/%s.bak", destination_folder,filename);
    if ((output = creat(name, 0660)) == -1)
    {
        close(input);
        return -1;
    }

    //Here we use kernel-space copying for performance reasons
    //sendfile will work with non-socket output (i.e. regular file) on Linux 2.6.33+
    off_t bytesCopied = 0;
    struct stat fileinfo = {0};
    fstat(input, &fileinfo);
    long result = sendfile(output, input, &bytesCopied, (size_t) fileinfo.st_size);

    close(input);
    close(output);
    free(name);
    free(source_name);
    return result;
}

static void handle_event(const char* sourceDir, const char* filename, const char* dst_folder_name, uint32_t mask) {
    if (detect_delete_extension(filename)) {
        log_info("Detected file with prefix : %s", filename);
        // glorious pointer arithmetic
        os_delete_file(sourceDir,"%s/%s",filename + 7);
        os_delete_file(sourceDir,"%s/delete_%s",filename + 7);
        os_delete_file(dst_folder_name,"%s/%s.bak",filename + 7);
        log_info("Deleted file %s", filename);
        return;
    }
    log_info("File modification/creation %s/%s", sourceDir, filename);
    if (mask & IN_CREATE) {
        os_copyfile(sourceDir,filename,dst_folder_name);
        log_info("Created file %s/%s", dst_folder_name, filename);
        return;
    } else if (mask & IN_DELETE){
        os_delete_file(sourceDir,"%s/%s",filename);
        os_delete_file(dst_folder_name,"%s/%s.bak",filename);
        log_info("Deleted file %s/%s", dst_folder_name, filename);
        return;
    }  else if (mask & IN_MODIFY) {
        os_copyfile(sourceDir,filename,dst_folder_name);
        log_info("Modified file %s/%s", dst_folder_name, filename);
        return;
    }
}


int initFSWatcherOnDir(int fd, const char *source) {
    int wd = inotify_add_watch(fd,source,IN_MODIFY | IN_CREATE | IN_MOVE | IN_DELETE);

    if(wd==-1){
        log_warn("Could not watch %s\n",source);
    }
    else{
        log_info("Watching directory %s\n", source);
    }
    return wd;
}


void directoryWorker(const char* src,const char* dst, int fd) {
    long length = 0;
    char buffer[BUF_LEN];

    while (1) {
        struct inotify_event *event, *end;
        length = read(fd, buffer, BUF_LEN);

        if (length < 0) {
            perror( "read" );
            break;
        }

        // Our current event pointer
        event = (struct inotify_event *) &buffer[0];
        // an end pointer so that we know when to stop looping below
        end = (struct inotify_event *) &buffer[length];

        while (event < end) {
            handle_event(src,event->name,dst, event->mask);
            // Now move to the next event
            event = (struct inotify_event *)((char*)event) + sizeof(*event) + event->len;
        }
    }

}