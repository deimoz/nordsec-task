### NordVPN home

### REQUIREMENTS

1. A working C11 gcc or clang compiler. gcc 11 was used during development. GCC 4.7 is the last supported version.
2. A Linux OS that has or can run cmake 3.19. Older versions would most likely work, just change the version in CMakeLists.txt, but this was not tested.

### BUILD

```
cmake .
make 
```

In case it doesn't work, a statically compiled executable is also added to the repository

### HOW TO USE

The help page is as such

```
Usage: nordsec [OPTION]...
Homework assignment.

  -h, --help                   Print help and exit
  -V, --version                Print version and exit
  -s, --sourceDir=STRING       Source folder path
  -d, --destinationDir=STRING  Destination folder path
  -l, --logfile=STRING         Log file path
  -q, --querydate=STRING       Log file date filter. Use an ISO conforming
                                 date, such as 2020-01-01
  -r, --queryregex=STRING      Log file filename regex filter. Only POSIX regex
                                 scans are supported

```
In general, the application will either accept one of the following combos as valid, otherwise throwing the help page:

1. -r and -q to parse log files
2. -h for help
3. -s and -d for the backup functionality

-l is optional and should work in all cases.