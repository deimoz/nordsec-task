//
// Created by deimoz on 8/19/21.
//

#ifndef NORDSEC_FSWATCHER_H
#define NORDSEC_FSWATCHER_H
int initFSWatcherOnDir(int fd, const char *source);
void directoryWorker(const char* src,const char* dst, int fd);
#endif //NORDSEC_FSWATCHER_H
